import Model.*;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;


public class Main extends Application {

    public Position sceneMaxValues;

    public Car createBlueCar(Group group) throws Exception {
        Car blueCar = new Car(new Rectangle(), new RectSize(64d, 32d), new Position(300d, 400d), true);
        group.getChildren().add(blueCar.getRect());
        return blueCar;
    }

    public Car createRedCar(Group group) throws Exception {
        Car redCar = new Car(new Rectangle(), new RectSize(64d, 32d), new Position(500d, 130d), false);
        group.getChildren().add(redCar.getRect());
        return redCar;
    }

    public Rectangle createBlueGate(Group group) {
        Rectangle blueGate = new Rectangle();
        blueGate.setX(224);
        blueGate.setWidth(192);
        blueGate.setY(618);
        blueGate.setHeight(22);
        blueGate.setFill(Color.BLUE);
        group.getChildren().add(blueGate);
        return blueGate;
    }

    public Rectangle createRedGate(Group group) {
        Rectangle redGate = new Rectangle();
        redGate.setX(224);
        redGate.setWidth(192);
        redGate.setY(0);
        redGate.setHeight(22);
        redGate.setFill(Color.RED);
        group.getChildren().add(redGate);
        return redGate;
    }

    public void displayer(int duration, Car car, Car car2, Position sceneMaxValues, Double angle, Ball ball, int fps) {
        DistanceCounter distanceCounter = new DistanceCounter();
        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(1d / fps), new EventHandler<ActionEvent>() {
            private int i = 0;
            @Override
            public void handle(ActionEvent event) {
                //System.out.println(i);
                if (i == 0) {
                    car.moveForward(angle, duration, distanceCounter);
                }
                if (car.getRect().getBoundsInParent().intersects(ball.getCircle().getBoundsInParent())) {
                    //System.out.println("piłka zderzona");
                    if (car.getAbilityName().equals("spikes")) {
                        ball.isMovementAllowed = false;
                        SpikesAbility spikesAbility = (SpikesAbility) car.getCurrentAbility();
                        if (spikesAbility.getBallHeld() == null) {
                            spikesAbility.setBallHeld(ball);
                        }
                        try {
                            spikesAbility.updateBallPosition(car.getCenterPosition(), car.angle);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {

                    }
                }
                if (car.getRect().intersects(car2.getRect().getBoundsInParent())) {
                    System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                    car2.isMoveAllowed = false;
                    car.isMoveAllowed = false;
                }
                if (car.getRect().getX() < 0 || car.getRect().getX() > sceneMaxValues.x) {
                    if (car.getRect().getY() < 0 || car.getRect().getY() > sceneMaxValues.y) {
                        car.moveForwardThread = null;
                        car.isMoveAllowed = false;
                        System.out.println("its Terminated");
                    }
                }
                if (car.isBlueTeam) {

                    if (distanceCounter.getDistanceTraveled() == 200 || distanceCounter.getDistanceTraveled() == 204) {
                        car.setAngle(45d);
                    }
//                    else if(distanceCounter.getDistanceTraveled() == 308d || distanceCounter.getDistanceTraveled() ==304d){
//                        car.setAngle(-45d);
//                    }
                    else {
                        System.out.println(distanceCounter.getDistanceTraveled());
                        ;
                    }
                }
                i++;
                if (i == (duration * fps) - 1) {
                    System.out.println("displayer TimeLine stopped");
                }
            }
        }));
        timeline.setCycleCount(duration * fps);
        timeline.play();
    }

    public void ballThreadExecutioner(Ball ball, Car car, Car car2, int timeRunning, int displayFps, int ballThreadFps) {
        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(1d / displayFps), new EventHandler<ActionEvent>() {
            private int i = 0;

            @Override
            public void handle(ActionEvent event) {
                ball.ballThread(car, car2, 5d, ballThreadFps);
                i++;
            }
        }));
        timeline.setCycleCount(timeRunning * displayFps);
        timeline.play();
    }

    @Override
    public void start(Stage stage) throws Exception {
        Path path = new Path();
        MoveTo moveTo = new MoveTo();
        path.getElements().add(moveTo);
        Group group = new Group(path);
        this.sceneMaxValues = new Position(640d, 640d);
        Scene scene = new Scene(group, sceneMaxValues.x, sceneMaxValues.y);
        scene.setFill(Color.BLACK);
        stage.setScene(scene);
        stage.show();
        stage.setTitle("SO2L");

        Ball ball = new Ball(new Circle(), 16, new Position(320d, 300d));
        group.getChildren().add(ball.getCircle());

        Rectangle blueGate = createBlueGate(group);
        Rectangle redGate = createRedGate(group);

        Car blueCar = this.createBlueCar(group);
        Car redCar = this.createRedCar(group);


        displayer(10, blueCar, redCar, sceneMaxValues, 0d, ball, 13);
        blueCar.setCurrentAbility(new SpikesAbility());
        displayer(10, redCar, blueCar, sceneMaxValues, -135d, ball, 17);
        ballThreadExecutioner(ball, blueCar, redCar, 4, 10,4);
    }

    public static void main(String[] args) {
        launch(args);
    }
}  