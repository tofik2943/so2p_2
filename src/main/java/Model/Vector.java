package Model;

public class Vector {
    public Double x;
    public Double y;

    public Vector(Double x, Double y) {
        this.x = x;
        this.y = y;
    }

    public Vector(Double angle) {
        this.x = Math.sin(angle * Math.PI / 180);
        this.y = Math.cos(angle * Math.PI / 180) * -1;
    }

    public Vector(Car car) throws Exception {
        if (car.angle != 0) {
            throw new Exception("To count car center car's angle must be 0");
        }
        this.x = car.getRect().getWidth() / 2;
        this.y = car.getRect().getHeight() / 2;
    }

    public void changeVectorLength(Double expectedLen) {
        Double actualLen = countLength();
        double ratio = expectedLen / actualLen;
        this.x *= ratio;
        this.y *= ratio;
    }

    public void updateLeashVectorForDeltaAngle(Double deltaAngle) {
        Double angleToCarCenter = Math.atan2(this.y, this.x) * 180 / Math.PI;
        Vector vectorByAngle = new Vector(angleToCarCenter + deltaAngle + 180);
        vectorByAngle.changeVectorLength(this.countLength());
        this.x = vectorByAngle.x;
        this.y = vectorByAngle.y;
    }


    public Double countLength(Double x, Double y) {
        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
    }

    public Double countLength() {
        Double x = this.x;
        Double y = this.y;
        return countLength(x, y);
    }
}
