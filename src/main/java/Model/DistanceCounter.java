package Model;

public class DistanceCounter {
    private Double distanceTraveled;

    public DistanceCounter(){
     this.distanceTraveled = 0d;
    }

    public void addDistance(Vector vector){
        distanceTraveled += vector.countLength();
    }

    public Double getDistanceTraveled(){
        return this.distanceTraveled;
    }
}
