package Model;

public class SpikesAbility extends Ability {

    public SpikesAbility() {
        super("spikes");
    }

    private Ball ballHeld;
    private Vector leash;
    private Double lastCarAngle;

    public void updateBallPosition(Position carPosition, Double carAngle) throws Exception {
        if (ballHeld != null) {
            if (this.leash == null) {
                setVector(carPosition, carAngle);
            } else {
                if (!carAngle.equals(lastCarAngle) && (lastCarAngle != null)) {
                    Double deltaCarAngle = lastCarAngle - carAngle;
                    this.leash.updateLeashVectorForDeltaAngle(deltaCarAngle);
                    this.lastCarAngle = carAngle;
                }
                ballHeld.setPosition(new Position(carPosition.x + leash.x,carPosition.y + leash.y));
            }
        } else {
            System.out.println("SpikesAbility: Ball not connected");
        }
    }

    private void setVector(Position carCenterPosition, Double carAngle) throws Exception {
        if (ballHeld != null) {
            Position ballPosition = this.ballHeld.getPosition();
            this.leash = new Vector(ballPosition.x - carCenterPosition.x, ballPosition.y - carCenterPosition.y);
            if (!carAngle.equals(lastCarAngle) && (lastCarAngle != null)) {
                System.out.println(carAngle + " " + lastCarAngle);
                throw new Exception("angle shouldn't changed here because its first execution");
            }
            this.lastCarAngle = carAngle;
        } else {
            System.out.println("SpikesAbility: Ball not connected");
        }
    }

    public void setBallHeld(Ball ballHeld) {
        this.ballHeld = ballHeld;
        this.setDuration(5);
    }

    public Ball getBallHeld() {
        return ballHeld;
    }
}
