package Model;

public abstract class Ability {
    private String name;
    private int duration;

    public String getAbilityName(){
        return name;
    }

    public Ability(String name) {
        this.name = name;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}
