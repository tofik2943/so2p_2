package Model;

public class RectSize {
    public Double height;
    public Double width;

    public RectSize(Double height, Double width) {
        this.height = height;
        this.width = width;
    }
}
