package Model;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

public class Car {
    private final Rectangle rect;
    private Ability currentAbility;
    public Double angle = 0d;
    public boolean isBlueTeam;
    public Thread moveForwardThread;
    public boolean isMoveAllowed = true;
    private Position centerPosition;

    public Rectangle getRect() {
        return this.rect;
    }

    public Car(Rectangle rect, RectSize rectSize, Position position, boolean isBlueTeam) throws Exception {
        super();
        this.rect = rect;
        setRectSize(rectSize);
        setPosition(position);
        setCenterPosition();
        if (isBlueTeam) {
            this.getRect().setFill(new ImagePattern(new Image("car_skin_blue.bmp")));
            this.isBlueTeam = true;
        } else {
            this.getRect().setFill(new ImagePattern(new Image("car_skin_red.bmp")));
            this.isBlueTeam = false;
        }
    }

    private void setPosition(Position pos) {
        this.rect.setX(pos.x);
        this.rect.setY(pos.y);
    }

    private void setCenterPosition() throws Exception {
        Vector vectorToCenter = new Vector(this);
        this.centerPosition = this.getDefaultPosition().addVector(vectorToCenter);
    }

    public Position getCenterPosition(){
        return this.centerPosition;
    }

    private void changePositionByVector(Vector vector) throws Exception {
        this.setPosition(this.getDefaultPosition().addVector(vector));
        updateCenterPosition(vector);
    }

    private void updateCenterPosition(Vector vector) throws Exception {
        if (this.centerPosition != null) {
            this.centerPosition = this.centerPosition.addVector(vector);
        } else throw new Exception("Car's center position is null!");
    }

    public Position getDefaultPosition() {
        Rectangle rectangle = this.getRect();
        return new Position(rectangle.getX(), rectangle.getY());
    }

    private void setRectSize(RectSize rectSize) {
        this.rect.setHeight(rectSize.height);
        this.rect.setWidth(rectSize.width);
    }

    public String getAbilityName() {
        if (currentAbility == null){
            return "None";
        }
        return this.currentAbility.getAbilityName();
    }

    public void setAngle(Double angle){
        this.angle = angle;
        this.rect.setRotate(angle);
    }


    public Ability getCurrentAbility() {
        return currentAbility;
    }

    public void moveForward(Double angle, double timeRunning ,DistanceCounter distanceCounter) {
        int fps = 17;
        setAngle(angle);
        this.moveForwardThread = new Thread(() -> {
            for (int i = 0; i < fps * timeRunning; i++) {
                try {
                    if (this.isMoveAllowed) {
                        Thread.sleep(1000 / fps);
                        Vector vector = new Vector(this.angle);
                        vector.changeVectorLength(4d);
                        changePositionByVector(vector);
                        distanceCounter.addDistance(vector);
                        //System.out.println("i: " + i + " isBlueTeam: " +isBlueTeam + " x: " + this.getRect().getX() + "\n" + "y: " + this.getRect().getY() + "\n");
                    }
                    //System.out.println(" is move allowed = " + isMoveAllowed);
                } catch (Exception e) {
                    System.out.println("interrupted");
                }
            }
            System.out.println("car: " + isBlueTeam + " move thread ended");
        });
        this.moveForwardThread.start();
    }

    public void setCurrentAbility(Ability ability) {
        this.currentAbility = ability;
    }
}
