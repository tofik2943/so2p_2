package Model;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;

import java.nio.DoubleBuffer;
import java.util.concurrent.atomic.AtomicReference;

public class Ball {
    private Circle circle;
    private int radius;
    public boolean isMovementAllowed;

    public Ball(Circle circle, int radius, Position position) {
        this.circle = circle;
        this.radius = radius;
        circle.setRadius(radius);
        setPosition(position);
        circle.setFill(new ImagePattern(new Image("ball_skin.bmp")));
        this.isMovementAllowed = true;
    }

    public Circle getCircle() {
        return this.circle;
    }

    public void setPosition(Position position) {
        circle.setCenterX(position.x);
        circle.setCenterY(position.y);
    }

    public Position getPosition() {
        return new Position(circle.getCenterX(), circle.getCenterY());
    }

    public Vector onCollisionCountVector(Car car, Double expectedVectorLength) {
        if (this.circle.getBoundsInParent().intersects(car.getRect().getBoundsInParent())) {
            Vector vector = new Vector(this.circle.getCenterX() - car.getCenterPosition().x,
                    this.circle.getCenterY() - car.getCenterPosition().y);
            vector.changeVectorLength(expectedVectorLength);
            return vector;
        }
        return null;
    }

    public void ballThread(Car car, Car car2, Double timeRunning,int fps) {
        Thread ballMovementThread = new Thread(() -> {
            Vector vectorGivenByCar = null;
            Double vectorLenOnCarHit = 8d;
            for (int i = 0; i < timeRunning * fps; i++) {
                if (isMovementAllowed) {
                    try {
                        Thread.sleep(1000 / fps);
                        Vector tempVector = onCollisionCountVector(car, vectorLenOnCarHit);
                        if (tempVector == null){
                            tempVector = onCollisionCountVector(car2, vectorLenOnCarHit);
                        }
                        if (tempVector != null){
                            if (vectorGivenByCar == null){
                                vectorGivenByCar = tempVector;
                            } else {
                                Double tempVectorLen = tempVector.countLength();
                                Double vectorGivenByCarLen = vectorGivenByCar.countLength();
                                tempVector.changeVectorLength(tempVectorLen + vectorGivenByCarLen);
                                vectorGivenByCarLen = tempVectorLen;
                            }
                        }
                        if (isMovementAllowed){
                            if (vectorGivenByCar != null){
                                if (vectorGivenByCar.countLength() != 0){
                                        this.setPosition(this.getPosition().addVector(vectorGivenByCar));
                                }
                            }
                        }
                        //rusz o główny Wektor

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        ballMovementThread.start();
    }
}
