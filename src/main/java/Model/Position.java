package Model;

public class Position {
    public Double x;
    public Double y;

    public Position(Double x, Double y) {
        this.x = x;
        this.y = y;
    }

    public Position addVector(Vector vector) {
        this.x += vector.x;
        this.y += vector.y;
        return new Position(this.x, this.y);
    }
}
